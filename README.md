# Content

[Setup environment](#setup-environment)

[Runtime data collection](#runtime-data-collection)

[Checking query unnesting](./checking_query_unnesting/README.md)

# Setup environment

- Create the file `/etc/yum.repos.d/MariaDB.repo` with the content below:
		
	```
	# MariaDB 10.5 CentOS repository list - created 2020-06-27 01:01 UTC
	# http://downloads.mariadb.org/mariadb/repositories/
	[mariadb]
	name = MariaDB
	baseurl = http://yum.mariadb.org/10.5/centos7-amd64
	gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
	gpgcheck=1
	```

- Run `yum install MariaDB-server MariaDB-client`
- Run `mariadb-upgrade` afer installation is complete to setup columnstore

# Runtime data collection

- Enable the performance schema within mariadb with:

	```
	mariadb> UPDATE performance_schema.setup_consumers SET ENABLED = 'YES';
	mariadb> UPDATE performance_schema.setup_instruments SET ENABLED = 'YES', TIMED = 'YES';
	```

- To get the runtime of a query from bash do:

	```
	mariadb -e 'truncate table performance_schema.events_statements_history_long'
	mariadb < file.sql >/dev/null
	profile_info=`mariadb -p -e "SELECT TRUNCATE(TIMER_WAIT/1000000000000,6) as Duration FROM performance_schema.events_statements_history_long WHERE SQL_TEXT LIKE '%select%'"`
	runtime=`awk '{print \$NF}' <<< $prodile_info`
	```
