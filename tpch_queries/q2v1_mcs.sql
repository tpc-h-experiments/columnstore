use tpch;
select
	s_acctbal,
	s_name,
	n_name,
	p_partkey,
	p_mfgr,
	s_address,
	s_phone,
	s_comment,
	ps_supplycost
from
	part,
	supplier,
	partsupp,
	nation,
	region,
	(
		select
			ps_partkey as mc_partkey,
			min(ps_supplycost) as mc_supplycost
		from
			partsupp,
			supplier,
			nation,
			region
		where
			s_suppkey = ps_suppkey
			and s_nationkey = n_nationkey
			and n_regionkey = r_regionkey
			and r_name = 'MIDDLE EAST'
		group by
			ps_partkey
	) min_costs
where
	p_partkey = ps_partkey
	and ps_partkey = mc_partkey
	and s_suppkey = ps_suppkey
	and p_size = 38
	and p_type like '%TIN'
	and s_nationkey = n_nationkey
	and n_regionkey = r_regionkey
	and r_name = 'MIDDLE EAST'
	and ps_supplycost = mc_supplycost
order by
	s_acctbal desc,
	n_name,
	s_name,
	p_partkey
LIMIT 100;
