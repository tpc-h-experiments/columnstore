use tpch
with psinfo as (
select 
	s_acctbal,
	s_name, 
       	n_name, 
       	p_partkey,
       	p_mfgr,
       	s_address,
       	s_phone,
       	s_comment,
       	ps_partkey,
       	ps_supplycost, 
       	p_size, p_type
from 
	part,
	supplier,
	partsupp,
	nation,
	region
where
	p_partkey = ps_partkey
	and s_suppkey = ps_suppkey
	and s_nationkey = n_nationkey
	and n_regionkey = r_regionkey
	and r_name = 'MIDDLE EAST')

select 
	s_acctbal,
	s_name,
	n_name,
	p_partkey,
	p_mfgr,
	s_address,
	s_phone,
	s_comment
from 
	psinfo,
	(select 
		ps_partkey as mc_partkey,
		min(ps_supplycost) as mc_supplycost
	from 
		psinfo
        group by 
	      	ps_partkey) as min_costs
where
	p_partkey = mc_partkey
	and ps_supplycost = mc_supplycost
	and p_size = 38
	and p_type like '%TIN'
order by
	s_acctbal desc,
	n_name,
	s_name,
	p_partkey
limit 100;
