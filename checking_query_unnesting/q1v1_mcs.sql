use tpch
select
	substring(c_phone from 1 for 2) as cntrycode,
	c_acctbal
from
	customer
where
	substring(c_phone from 1 for 2) in
		('30', '17', '25', '10', '22', '15', '21')
	and c_acctbal > (
		select
			avg(c_acctbal)
		from
			customer
		where
			c_acctbal > 0.00
			and substring(c_phone from 1 for 2) in
				('30', '17', '25', '10', '22', '15', '21')
	)
	and not exists (
		select
			*
		from
			orders
		where
			o_custkey = c_custkey
	)
order by
      cntrycode desc,
      c_acctbal desc
