# Checking query unnesting

## Hypotheses

- **Query unnnesting with columns**. MariaDB does not unnest subqueries, but
   considers unnesting when producing the final plan (not exists => antijoin).
- **Order of evaluation**. When a selection involves a subquery, it is 
  difficult to estimate selectivity and cost. We think most systems 
  (even relational ones) keep such selections to a fixed position. 

## Experiments

- **Query unnnesting with columns**. We check this using [Q1v1](./q1v1_mcs.sql), with a correlated (using `not exists`) and uncorrelated subquery, and [Q1v2](./q1v2_mcs.sql), an alternative version unnnested manually.
- **Order of evaluation**. We check this by submitting a query to Postgres over a single table, with
  some simple selections (just involving constants) and one selection that
  involves an aggregated, non correlated subquery 
  (since those cannot be unnested), then submit the same query with the 
  selections moved around, and compare query plans to see in what order the
  selections are done. We may need to repeat this changing the constants, to see if the selection order changes

## Results

**Q1v1 (uses not exists)**

**Query trace**

```
Desc Mode Table               TableOID ReferencedColumns             PIO LIO  PBE Elapsed Rows    
BPS  PM   orders              4297     (o_custkey)                   0   737  0   0.050   1500000 
TNS  UM   -                   -        -                             -   -    -   0.066   1500000 
BPS  PM   customer            4283     (c_acctbal,c_phone)           0   773  0   0.068   19      
TAS  UM   -                   -        -                             -   -    -   0.012   1       
TNS  UM   -                   -        -                             -   -    -   0.000   1       
BPS  PM   customer            4283     (c_acctbal,c_custkey,c_phone) 0   1397 0   0.186   6281    
HJS  PM   customer-$sub_1_1_1 4283     -                             -   -    -   -----   -       
TNS  UM   -                   -        -                             -   -    -   0.344   6281
```

**Runtime:** 944.018ms (at 1GB scale factor)

**Q1v2 (replaces not exists with left join)**

**Query trace**

```
Desc Mode Table           TableOID ReferencedColumns             PIO LIO  PBE Elapsed Rows   
BPS  PM   customer        4283     (c_acctbal,c_phone)           0   773  0   0.090   19     
TAS  UM   -               -        -                             -   -    -   0.013   1      
TNS  UM   -               -        -                             -   -    -   0.000   1      
BPS  PM   customer        4283     (c_acctbal,c_custkey,c_phone) 0   1324 0   0.050   18748  
BPS  PM   orders          4297     (o_custkey)                   0   737  0   0.051   187432 
HJS  PM   orders-customer 4297     -                             -   -    -   -----   -      
TNS  UM   -               -        -                             -   -    -   0.123   6281
```

**Runtime:** 603.825 (at 1GB scale factor)

## Analysis

The join translation of the "not exists" is done in both queries at the end. Q1v1 seems to scan all orders, as it computes the subquery within the "not exists" first, and then filters them by customer. Q1v2 filters customers first and then scans orders. The HJS (hash-join) step in Q1v2 is applied between tables orders and customer, but Q1v1 does it on customer and a temporary table created after scanning and filtering orders.
