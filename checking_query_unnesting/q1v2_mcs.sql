use tpch
select
	substring(c_phone from 1 for 2) as cntrycode,
	c_acctbal
from
	customer left join orders
	on
	o_custkey = c_custkey
where
	substring(c_phone from 1 for 2) in
		('30', '17', '25', '10', '22', '15', '21')
	and c_acctbal > (
		select
			avg(c_acctbal)
		from
			customer
		where
			c_acctbal > 0.00
			and substring(c_phone from 1 for 2) in
				('30', '17', '25', '10', '22', '15', '21')
	)
	and o_custkey is null
order by
      cntrycode desc,
      c_acctbal desc
