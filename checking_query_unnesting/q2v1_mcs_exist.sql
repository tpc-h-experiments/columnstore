use tpch
select
	substring(c_phone from 1 for 2) as cntrycode,
	c_acctbal,
	avg(o_totalprice)
from
	customer join orders on c_custkey = o_orderkey
where
	substring(c_phone from 1 for 2) in
		('30', '17', '25', '10', '22', '15', '21')
	and c_acctbal > (
		select
			avg(c_acctbal)
		from
			customer
		where
			c_acctbal > 0.00
			and substring(c_phone from 1 for 2) in
				('30', '17', '25', '10', '22', '15', '21')
	)
	and o_totalprice > (
		select
			avg(o_totalprice)
		from
			orders
		where
			o_custkey = c_custkey
			and o_orderdate in (
			    -- This could be done much easier by just using "limit"
			    -- but MariaDB columnstore does not support limit
			    -- within subqueries as of now
			    select 
			    	   o1.o_orderdate 
			    from orders o1 left join orders o2 
			    	  on o1.o_custkey = o2.o_custkey 
			    	  and o1.o_orderdate < o2.o_orderdate 
			    where 
			    	  o1.o_custkey = c_custkey
			    group by
			    	  o1.o_orderdate
			    having 
			    	  count(*) < 3
					
			)
		group by 
		      	o_custkey
	)
group by
      c_custkey,
      cntrycode,
      c_acctbal
order by
      cntrycode desc,
      c_acctbal desc

